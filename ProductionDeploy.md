# Deploying to a Production Environment

### 1. Setting Production-Specific Properties
You have seen how properties in ```src/main/resources/application.properties``` may be overridden by environmental variables or command-line variables at runtime (e.g. we set DB_HOST to change the mongo host from localhost to something else).

We can also change the entire properties file that is to be used by the running application. This allows us to have multiple properties files in ```src/main/resources/```, each pertaining to a different deployment environment.

For example, make the following 2 updates to your Java codebase(s):
1. Add this line to the top of  ```src/main/resources/application.properties```:
```
spring.profiles.active=${PROFILE:default}
```

2. Create a new properties file called ```src/main/resources/application-prod.properties```, in this file we will use a mongo connection string that requires a username and password for mongo.

***MAKE SURE YOU REPLACE <your-team-name> below with a unique name for your team e.g. nh2-tradedb. As multiple teams may be using the same mongo host, and this will avoid any accidental read/write of another team's data.

```
spring.data.mongodb.uri = mongodb://${DB_USER}:${DB_PASS}@${DB_HOST:localhost}/${AUTH_DB:admin}

spring.data.mongodb.database=${DB_NAME:<your-team-name>}

server.port=${SERVER_PORT:8080}

logging.level.root=${ROOT_LOG_LEVEL:WARN}
```

Note that in the connection string the ```AUTH_DB``` tells mongo which database to authenticate against (commonly admin). While ```spring.data.mongodb.database``` is indicating which mongo database to actually read/write to. See the mongo docs for "authentication-database" for more info about this mongo detail.

You can now switch between the two properties files at runtime. Setting the environmental/command-line variable ```PROFILE=prod``` will use ```application-prod.properties```. Otherwise your app will default to ```application.properties```.

Commonly a spring boot app will have a number of properties files for different environments.

### 2. Updating the Jenkins Pipeline

If your Jenkins pipeline is correctly bringing up a container on your linux machine, and you have the properties changes above made in your latest build, then you could add some extra steps to your Jenkins pipeline.

Consider that the container running on your linux machine is acting your ```Dev``` deployment environment. This would be the first environment of a sequence that your containers would be deployed to. Each environment would have a different level/style of integration and system testing applied to it.

For your Dev environment, your Jenkins pipeline could include one or more steps to verify that the Dev deployment is running correctly e.g.:

1. Do a HTTP GET against your TradeAPI and verify it returns HTTP 200, e.g.
```
    stage('Test Deployed Dev Container') {
      steps {
        sh 'curl -f -X GET "http://citieurlinuxinst1.conygre.com:8080/v1/trade" -H "accept: */*"'
      }
    }
```

2. If the above works you could try to add more tests to the same step as more ```sh 'curl....'``` lines. The swagger-ui will show you the curl command for each of your API operations.

3. You could now add a Jenkins step to push your container to dockerhub. From there it can be deployed to further environments. To do this you will need a dockerhub account. If you are ready to do this step, ask your instructor to tell you how to get Jenkins correctly authenticated with your dockerhub account. In this case, dockerhub is performing the same role as artifactory does within Citi.

Replace <YOUR_DOCKER_ID> with your actual docker account username.
```
    stage('Save Image To Dockerhub') {
      steps {
          sh "docker tag ${projectName}:0.0.${currentBuild.number} <YOUR_DOCKER_ID>/${projectName}:0.0.${currentBuild.number}"
          sh "docker push <YOUR_DOCKER_ID>/${projectName}:0.0.${currentBuild.number}"
      }
    }
```

4. If you have a docker images successfully pushed to dockerhub then notify you instructor as that/those images can be deployed to openshift.